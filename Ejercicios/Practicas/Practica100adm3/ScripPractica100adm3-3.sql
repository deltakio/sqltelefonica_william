use sae_William;
go
-- Punto 2 --
CREATE VIEW provinciasPartidos_view
AS 
SELECT PROV_NOMBRE,PART_NOMBRE
FROM provincias AS pro INNER JOIN partidos AS par
ON pro.PROV_ID=par.PROV_ID;

select * from provinciasPartidos_view

-- Punto 3 --

use sae_William;
go
CREATE VIEW nombreApellidoPartido_view
AS 
SELECT ALU_NOMBRE,ALU_APELLIDO,PART_NOMBRE
FROM alumnos AS ALU INNER JOIN partidos AS par
ON ALU.PART_ID=par.PART_ID;

select * from nombreApellidoPartido_view

-- Punto 4 -- 

use sae_William;
go
CREATE VIEW alumnosPArtidosProvincias_view
AS 
SELECT ALU_NOMBRE,ALU_APELLIDO,PART_NOMBRE, PROV_NOMBRE
FROM alumnos AS ALU INNER JOIN partidos AS par
ON ALU.PART_ID=par.PART_ID
inner join provincias as PROV
on PAR.PROV_ID=PROV.PROV_ID

select * from alumnosPArtidosProvincias_view

-- Punto 5 --
USE sae_William
go
create view turnodivisionprofalum_view
as
select COM_TURNO, COM_DIVISION,PROF_NOMBRE, PROF_APELLIDO, ALU_NOMBRE, ALU_APELLIDO
from comisiones as COM inner join profesores as PROF
on COM.PROF_ID=PROF.PROF_ID
inner join alumnos as ALU
on ALU.COM_ID=COM.COM_ID;

select * from turnodivisionprofalum_view

