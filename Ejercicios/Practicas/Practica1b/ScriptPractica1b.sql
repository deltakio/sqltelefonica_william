use cine_William
--Crear tabla Generos Punto 3--
CREATE TABLE Generos(
	GEN_ID INT NOT NULL IDENTITY(1,1),
	GEN_DESCRIPCION VARCHAR(50),
	CONSTRAINT PK_GEN PRIMARY KEY(GEN_ID)
	);
--Crear tabla Peliculas Punto 3--
CREATE TABLE Peliculas(
	PEL_ID INT NOT NULL IDENTITY(1,1),
	GEN_ID INT NOT NULL,
	PEL_DESCRIPCION VARCHAR(50),
	CONSTRAINT PK_PEL PRIMARY KEY(PEL_ID),
	CONSTRAINT FK_GEN_ID FOREIGN KEY (GEN_ID) REFERENCES Generos (GEN_ID)
	);

-- Insertar datos tabla Generos Punto 4 --
SET IDENTITY_INSERT Generos ON
INSERT INTO Generos (GEN_ID, GEN_DESCRIPCION) VALUES (1, N'TERROR')
INSERT INTO Generos (GEN_ID, GEN_DESCRIPCION) VALUES (2, N'DRAMA')
INSERT INTO Generos (GEN_ID, GEN_DESCRIPCION) VALUES (3, N'ACCION')
INSERT INTO Generos (GEN_ID, GEN_DESCRIPCION) VALUES (4, N'DOCUMENTAL')
INSERT INTO Generos (GEN_ID, GEN_DESCRIPCION) VALUES (5, N'COMEDIA')
SET IDENTITY_INSERT Generos OFF

-- Insertar datos tabla Peliculas Punto 4 --
SET IDENTITY_INSERT Peliculas ON
INSERT INTO Peliculas (PEL_ID,GEN_ID, PEL_DESCRIPCION) VALUES (1, 5, N'LA PISTOLA DESNUDA')
INSERT INTO Peliculas (PEL_ID,GEN_ID, PEL_DESCRIPCION) VALUES (2, 3, N'GLADIADOR')
INSERT INTO Peliculas (PEL_ID,GEN_ID, PEL_DESCRIPCION) VALUES (3, 2, N'TITANIC')
INSERT INTO Peliculas (PEL_ID,GEN_ID, PEL_DESCRIPCION) VALUES (4, 5, N'RAMBITO Y RAMBON')
INSERT INTO Peliculas (PEL_ID,GEN_ID, PEL_DESCRIPCION) VALUES (6, 1, N'LA PROFECIA')
INSERT INTO Peliculas (PEL_ID,GEN_ID, PEL_DESCRIPCION) VALUES (7, 5, N'LOS BA�EROS MAS LOCOS DEL MUNDO')
INSERT INTO Peliculas (PEL_ID,GEN_ID, PEL_DESCRIPCION) VALUES (8, 1, N'EL ESPLANDOR')
INSERT INTO Peliculas (PEL_ID,GEN_ID, PEL_DESCRIPCION) VALUES (9, 3, N'BATMAN')
INSERT INTO Peliculas (PEL_ID,GEN_ID, PEL_DESCRIPCION) VALUES (10, 3, N'MISION IMPOSIBLE')
INSERT INTO Peliculas (PEL_ID,GEN_ID, PEL_DESCRIPCION) VALUES (11, 3, N'ENCUENTRO EXPLOSIVO')
INSERT INTO Peliculas (PEL_ID,GEN_ID, PEL_DESCRIPCION) VALUES (13, 3, N'TROYA')
INSERT INTO Peliculas (PEL_ID,GEN_ID, PEL_DESCRIPCION) VALUES (14, 1, N'LA ISLA SINIESTRA')
SET IDENTITY_INSERT Peliculas OFF


-- Crear tablas peliculas-actores y actores Punto 6 --
CREATE TABLE actores(
	ACT_ID INT NOT NULL IDENTITY(1,1),
	ACT_NOMBRE VARCHAR(50),
	ACT_APELLIDO VARCHAR(50),
	CONSTRAINT PK_ACT PRIMARY KEY(ACT_ID)
	);

CREATE TABLE Peliculas_Actores(
    PEL_ID INT NOT NULL,
	ACT_ID INT NOT NULL,
	CONSTRAINT PF_COMPUESTA_PEL_ACT PRIMARY KEY(PEL_ID,ACT_ID),
    CONSTRAINT FK_PEL_ID FOREIGN KEY(PEL_ID) REFERENCES Peliculas(PEL_ID),
    CONSTRAINT FK_ACT_ID FOREIGN KEY(ACT_ID) REFERENCES actores(ACT_ID)
	);

-- Insertar datos tabla actores Punto 7--
SET IDENTITY_INSERT actores ON
INSERT INTO actores (ACT_ID, ACT_NOMBRE, ACT_APELLIDO) VALUES (1, N'Jack', N'Nicholson')
INSERT INTO actores (ACT_ID, ACT_NOMBRE, ACT_APELLIDO) VALUES (2, N'Brad', N'Pit')
INSERT INTO actores (ACT_ID, ACT_NOMBRE, ACT_APELLIDO) VALUES (3, N'Tom', N'Hanks')
INSERT INTO actores (ACT_ID, ACT_NOMBRE, ACT_APELLIDO) VALUES (4, N'Tom', N'Cruise')
INSERT INTO actores (ACT_ID, ACT_NOMBRE, ACT_APELLIDO) VALUES (5, N'Leonardo', N'Di Caprio')
INSERT INTO actores (ACT_ID, ACT_NOMBRE, ACT_APELLIDO) VALUES (6, N'Leslie', N'Nielsen')
INSERT INTO actores (ACT_ID, ACT_NOMBRE, ACT_APELLIDO) VALUES (7, N'Russel', N'Crowe')
INSERT INTO actores (ACT_ID, ACT_NOMBRE, ACT_APELLIDO) VALUES (8, N'Naomi', N'Watts')
INSERT INTO actores (ACT_ID, ACT_NOMBRE, ACT_APELLIDO) VALUES (9, N'Gregory', N'Peck')
INSERT INTO actores (ACT_ID, ACT_NOMBRE, ACT_APELLIDO) VALUES (10, N'Emilio', N'Disi')
INSERT INTO actores (ACT_ID, ACT_NOMBRE, ACT_APELLIDO) VALUES (11, N'Alberto', N'Fernandez de Rosa')
INSERT INTO actores (ACT_ID, ACT_NOMBRE, ACT_APELLIDO) VALUES (12, N'Guillermo', N'Francella')
INSERT INTO actores (ACT_ID, ACT_NOMBRE, ACT_APELLIDO) VALUES (13, N'Mario', N'Castiglioni')
INSERT INTO actores (ACT_ID, ACT_NOMBRE, ACT_APELLIDO) VALUES (14, N'Monica', N'Gonzaga')
INSERT INTO actores (ACT_ID, ACT_NOMBRE, ACT_APELLIDO) VALUES (15, N'Alberto', N'Olmedo')
INSERT INTO actores (ACT_ID, ACT_NOMBRE, ACT_APELLIDO) VALUES (16, N'Jorge', N'Porcel')
SET IDENTITY_INSERT actores OFF

-- Insertar datos tabla actores Punto 8--
INSERT INTO Peliculas_Actores  (PEL_ID, ACT_ID) VALUES (1, 6)
INSERT INTO Peliculas_Actores  (PEL_ID, ACT_ID) VALUES (2, 7)
INSERT INTO Peliculas_Actores  (PEL_ID, ACT_ID) VALUES (3, 5)
INSERT INTO Peliculas_Actores  (PEL_ID, ACT_ID) VALUES (4, 15)
INSERT INTO Peliculas_Actores  (PEL_ID, ACT_ID) VALUES (4, 16)
INSERT INTO Peliculas_Actores  (PEL_ID, ACT_ID) VALUES (5, 8)
INSERT INTO Peliculas_Actores  (PEL_ID, ACT_ID) VALUES (6, 9)
INSERT INTO Peliculas_Actores  (PEL_ID, ACT_ID) VALUES (7, 10)
INSERT INTO Peliculas_Actores  (PEL_ID, ACT_ID) VALUES (7, 11)
INSERT INTO Peliculas_Actores  (PEL_ID, ACT_ID) VALUES (7, 12)
INSERT INTO Peliculas_Actores  (PEL_ID, ACT_ID) VALUES (7, 13)
INSERT INTO Peliculas_Actores  (PEL_ID, ACT_ID) VALUES (7, 14)
INSERT INTO Peliculas_Actores  (PEL_ID, ACT_ID) VALUES (8, 1)
INSERT INTO Peliculas_Actores  (PEL_ID, ACT_ID) VALUES (9, 1)
INSERT INTO Peliculas_Actores  (PEL_ID, ACT_ID) VALUES (10, 4)
INSERT INTO Peliculas_Actores  (PEL_ID, ACT_ID) VALUES (11, 4)
INSERT INTO Peliculas_Actores  (PEL_ID, ACT_ID) VALUES (12, 2)
INSERT INTO Peliculas_Actores  (PEL_ID, ACT_ID) VALUES (13, 2)
INSERT INTO Peliculas_Actores  (PEL_ID, ACT_ID) VALUES (14, 5)

-- Modificar Registro Primera Mayuscula--
UPDATE Generos
set GEN_DESCRIPCION = UPPER(SUBSTRING(GEN_DESCRIPCION,1,1)) + LOWER(SUBSTRING(GEN_DESCRIPCION,2,LEN(GEN_DESCRIPCION)))
where GEN_ID = '1';
UPDATE Generos
set GEN_DESCRIPCION = UPPER(SUBSTRING(GEN_DESCRIPCION,1,1)) + LOWER(SUBSTRING(GEN_DESCRIPCION,2,LEN(GEN_DESCRIPCION)))
where GEN_ID = '2';
UPDATE Generos
set GEN_DESCRIPCION = UPPER(SUBSTRING(GEN_DESCRIPCION,1,1)) + LOWER(SUBSTRING(GEN_DESCRIPCION,2,LEN(GEN_DESCRIPCION)))
where GEN_ID = '3';
UPDATE Generos
set GEN_DESCRIPCION = UPPER(SUBSTRING(GEN_DESCRIPCION,1,1)) + LOWER(SUBSTRING(GEN_DESCRIPCION,2,LEN(GEN_DESCRIPCION)))
where GEN_ID = '4';
UPDATE Generos
set GEN_DESCRIPCION = UPPER(SUBSTRING(GEN_DESCRIPCION,1,1)) + LOWER(SUBSTRING(GEN_DESCRIPCION,2,LEN(GEN_DESCRIPCION)))
where GEN_ID = '5';
