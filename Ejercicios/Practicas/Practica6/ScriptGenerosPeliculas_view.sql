USE cine
GO
create view GenerosPeliculas_view
as
SELECT GEN_DESCRIPCION, PEL_DESCRIPCION
FROM generos AS GEN
INNER JOIN peliculas as PEL
ON GEN.GEN_ID = PEL.GEN_ID


select * from GenerosPeliculas_view