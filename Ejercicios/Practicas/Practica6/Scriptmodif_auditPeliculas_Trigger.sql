use cine;

--Crea tabla auditPeliculas
create table auditPeliculas(
PEL_ID int null,
PEL_DESCRIPCION varchar(50) null,
user_name char(16) null,
date datetime null,
PEL_DESCRIPCION_old varchar(50) null,
PEL_DESCRIPCION_new varchar(50) null
);


create trigger modif_auditPeliculas_Trigger
ON dbo.peliculas AFTER UPDATE
AS IF UPDATE(PEL_DESCRIPCION)
BEGIN
DECLARE @PEL_DESCRIPCION_old varchar(50)
DECLARE @PEL_DESCRIPCION_new varchar(50)
DECLARE @PEL_ID INT
SELECT @PEL_DESCRIPCION_old = (SELECT PEL_DESCRIPCION FROM deleted)
SELECT @PEL_DESCRIPCION_new = (SELECT PEL_DESCRIPCION FROM inserted)
SELECT @PEL_ID = (SELECT PEL_ID FROM inserted)
insert into auditPeliculas values 
(@PEL_ID, USER_NAME(), GETDATE(), @PEL_DESCRIPCION_old, @PEL_DESCRIPCION_new)
END

select * from auditPeliculas
select * from peliculas

UPDATE peliculas
SET PEL_DESCRIPCION ='ROCKY IV'
WHERE PEL_ID=4;

select * from auditProvincia