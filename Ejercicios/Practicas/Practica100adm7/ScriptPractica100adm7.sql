use cine
-- Punto 31 --
CREATE FUNCTION FN_getCantidadDePeliculasPorGenero(@GEN_ID INT) 
RETURNS INT
AS
BEGIN
   DECLARE @CANT_POR_GENERO INT
   SELECT @CANT_POR_GENERO = COUNT(GEN_ID) FROM peliculas WHERE GEN_ID=@GEN_ID;
   RETURN @CANT_POR_GENERO
END

--Punto 32--
print dbo.FN_getCantidadDePeliculasPorGenero(1)
print dbo.FN_getCantidadDePeliculasPorGenero(2)
print dbo.FN_getCantidadDePeliculasPorGenero(3)
print dbo.FN_getCantidadDePeliculasPorGenero(4)
print dbo.FN_getCantidadDePeliculasPorGenero(5)

--Punto 33 --
CREATE FUNCTION FN_getDescripcionDeGenero(@GEN_ID INT) 
RETURNS VARCHAR(50)
AS
BEGIN
   DECLARE @GEN_DESC VARCHAR(50)
   SELECT @GEN_DESC = GEN_DESCRIPCION
					  FROM peliculas AS PEL
					  INNER JOIN generos AS GEN ON GEN.GEN_ID=PEL.GEN_ID
					  WHERE GEN.GEN_ID=@GEN_ID;
   RETURN @GEN_DESC
END

-- Punto 34 --
PRINT 'GENERO:' + dbo.FN_getDescripcionDeGenero(1) + ' - CANTIDAD:' + CAST(dbo.FN_getCantidadDePeliculasPorGenero(1) AS VARCHAR(10));
PRINT 'GENERO:' + dbo.FN_getDescripcionDeGenero(2) + ' - CANTIDAD:' + CAST(dbo.FN_getCantidadDePeliculasPorGenero(2) AS VARCHAR(10));
PRINT 'GENERO:' + dbo.FN_getDescripcionDeGenero(3) + ' - CANTIDAD:' + CAST(dbo.FN_getCantidadDePeliculasPorGenero(3) AS VARCHAR(10));
PRINT 'GENERO:' + dbo.FN_getDescripcionDeGenero(5) + ' - CANTIDAD:' + CAST(dbo.FN_getCantidadDePeliculasPorGenero(5) AS VARCHAR(10));


--Punto 35 --
CREATE FUNCTION FN_getPeliculasPorGenero(@GEN_ID INT) 
RETURNS TABLE AS
RETURN (
SELECT PEL_ID,GEN_ID,PEL_DESCRIPCION,PEL_IMDB
FROM peliculas
WHERE GEN_ID=@GEN_ID
);

-- Punto 36--
SELECT * FROM FN_getPeliculasPorGenero(5)

-- Punto 37 --
INSERT INTO generos (GEN_DESCRIPCION) VALUES ('AVENTURA');
INSERT INTO generos (GEN_DESCRIPCION) VALUES ('CIENCIA FICCION');
INSERT INTO generos (GEN_DESCRIPCION) VALUES ('POLICIAL');

SELECT * FROM generos

-- Punto 38
SELECT GEN.GEN_ID,GEN.GEN_DESCRIPCION,PEL_DESCRIPCION
FROM peliculas AS PEL
INNER JOIN generos AS GEN ON GEN.GEN_ID=PEL.GEN_ID
WHERE GEN.GEN_ID=5;

-- Punto39 --
SELECT GEN.GEN_ID,GEN.GEN_DESCRIPCION,PEL_DESCRIPCION
FROM generos AS GEN
CROSS APPLY FN_getPeliculasPorGenero(5) PEL
WHERE GEN.GEN_ID=5;

--Punto 40 --
SELECT GEN.GEN_ID,GEN.GEN_DESCRIPCION,PEL_ID,PEL_DESCRIPCION,PEL_IMDB
FROM peliculas AS PEL
RIGHT JOIN generos AS GEN ON GEN.GEN_ID=PEL.GEN_ID;

--Punto 41 --
SELECT GEN.GEN_ID,GEN.GEN_DESCRIPCION,PEL.PEL_ID,PEL.PEL_DESCRIPCION,PEL.PEL_IMDB
FROM generos AS GEN
OUTER APPLY FN_getPeliculasPorGenero(GEN.GEN_ID) PEL;

