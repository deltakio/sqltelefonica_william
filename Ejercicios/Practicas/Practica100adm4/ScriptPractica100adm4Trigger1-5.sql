use db_William;
--Se crea la tabla Project
create table project(
project_nro char(4) primary key (project_nro),
budget float
) ;

--Se crea la tabla auditoria y un trigger
use db_William;
go
create table audit_budge(
project_no char(4) null,
user_name char(16) null,
date datetime null,
budget_old float null,
budgt_new float null
);
go
create trigger modify_budget
on dbo.project AFTER UPDATE
AS IF UPDATE(BUDGET)
BEGIN
DECLARE @BUDGET_OLD FLOAT
DECLARE @BUDGET_NEW FLOAT
DECLARE @PROJECT_NUMBER CHAR(4)
SELECT @BUDGET_OLD = (SELECT BUDGET FROM deleted)
SELECT @BUDGET_NEW = (SELECT BUDGET FROM inserted)
SELECT @PROJECT_NUMBER = (SELECT project_nro FROM inserted)
insert into audit_budge values 
(@PROJECT_NUMBER, USER_NAME(), GETDATE(), @BUDGET_OLD, @BUDGET_NEW)
END

--Se insertan los datos--
use db_William;
insert into project values('p001',137.18),
('p002',36.75),
('p003',350.7),
('p004',233.45),
('p005',140.9),
('p006',230.15),
('p007',145.7);

select * from project

--Modificar un dato de la tabla Project
use db_William;
update project 
set budget =2500
where project_nro='p003';

select * from project

use db_William;
select * from audit_budge
