-----------------BEGIN: Script to be run at Publisher 'T201805'-----------------
use [cine]
exec sp_addsubscription @publication = N'rep_generoPelicula', @subscriber = N'T201805\MATIAS2015', @destination_db = N'cine_Matias', @subscription_type = N'Push', @sync_type = N'automatic', @article = N'all', @update_mode = N'read only', @subscriber_type = 0
exec sp_addpushsubscription_agent @publication = N'rep_generoPelicula', @subscriber = N'T201805\MATIAS2015', @subscriber_db = N'cine_Matias', @job_login = null, @job_password = null, @subscriber_security_mode = 1, @frequency_type = 1, @frequency_interval = 0, @frequency_relative_interval = 0, @frequency_recurrence_factor = 0, @frequency_subday = 0, @frequency_subday_interval = 0, @active_start_time_of_day = 0, @active_end_time_of_day = 0, @active_start_date = 0, @active_end_date = 19950101, @enabled_for_syncmgr = N'False', @dts_package_location = N'Distributor'
GO
use [cine]
exec sp_addsubscription @publication = N'rep_generoPelicula', @subscriber = N'T201805\NICOLAS2017', @destination_db = N'cine_Nicolas', @subscription_type = N'Push', @sync_type = N'automatic', @article = N'all', @update_mode = N'read only', @subscriber_type = 0
exec sp_addpushsubscription_agent @publication = N'rep_generoPelicula', @subscriber = N'T201805\NICOLAS2017', @subscriber_db = N'cine_Nicolas', @job_login = null, @job_password = null, @subscriber_security_mode = 1, @frequency_type = 1, @frequency_interval = 0, @frequency_relative_interval = 0, @frequency_recurrence_factor = 0, @frequency_subday = 0, @frequency_subday_interval = 0, @active_start_time_of_day = 0, @active_end_time_of_day = 0, @active_start_date = 0, @active_end_date = 19950101, @enabled_for_syncmgr = N'False', @dts_package_location = N'Distributor'
GO
-----------------END: Script to be run at Publisher 'T201805'-----------------

