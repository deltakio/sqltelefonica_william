drop procedure sp_modificarImdb
use cine;
go
-- Crear Stored Procedure modifica pel_imdb con pel_id --
create procedure sp_modificarImdb(@pel_id int, @pel_imdb float)
AS
  BEGIN TRY
     BEGIN TRANSACTION
	    IF (@pel_imdb <= 10)
		  BEGIN
            update  peliculas 
            set pel_imdb = @pel_imdb 
            WHERE PEL_ID = @pel_id
            print ' La calificación es válida'
	        COMMIT TRANSACTION
          END
        ELSE IF (@pel_imdb > 10)
		   BEGIN
		       print 'ERROR La calificación de la Pelicula es mayor a 10'
		   END 
  END TRY
  BEGIN CATCH 
		print 'La calificación de la Pelicula es mayor a 10'
		ROLLBACK 
  END CATCH;
         

use cine
execute  sp_modificarImdb 1 , 7
execute  sp_modificarImdb 2 , 9
execute  sp_modificarImdb 3 , 5.8
execute  sp_modificarImdb 4 , 9.2
execute  sp_modificarImdb 5,  3.1

select * from peliculas
-- UPDATE peliculas set pel_imdb = NULL





