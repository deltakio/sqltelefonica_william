RESTORE DATABASE sae_William
  FROM DISK = 'C:\backup\sae_William.bak'
  WITH REPLACE;

  INSERT INTO alumnos(PART_ID,COM_ID,ALU_NOMBRE,ALU_APELLIDO,ALU_TELEFONO,ALU_DNI,ALU_EMAIL) VALUES 
 (100,5,'William','Erazo','su casa',NULL,NULL);


 BACKUP DATABASE sae_William
	TO DISK ='C:\backup\sae_William.bak_diferencial.dif'
	WITH INIT, DIFFERENTIAL, COMPRESSION;

BACKUP LOG sae_William
	TO DISK ='C:\backup\sae_William.bak_log.log'
	WITH INIT, COMPRESSION; 

	BACKUP DATABASE sae_William
	TO DISK ='C:\backup\sae_William.bak_sin_compresion.bak'
	WITH INIT;

	RESTORE DATABASE sae_William
	FROM DISK = 'C:\backup\sae_William.bak'
	WITH REPLACE, NORECOVERY;


GO	

RESTORE DATABASE sae_William
	FROM DISK = 'C:\backup\sae_William.bak_diferencial.dif'
	WITH RECOVERY;
GO



