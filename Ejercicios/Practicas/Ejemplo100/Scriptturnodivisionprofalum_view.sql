create view materiasCantidadModulos_view
as
SELECT MAT_NOMBRE, COUNT (MOD_DESCRIPCION) AS CANT_MODULOS
FROM materias AS MAT
INNER JOIN modulos MODU ON MODU.MAT_ID=MAT.MAT_ID
GROUP BY MAT.MAT_NOMBRE
/* HAVING COUNT (MOD_DESCRIPCION)>3; */

select * from materiasCantidadModulos_view where CANT_MODULOS > 5;