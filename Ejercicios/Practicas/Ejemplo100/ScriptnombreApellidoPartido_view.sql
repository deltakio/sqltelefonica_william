use sae_William;
go
CREATE VIEW nombreApellidoPartido_view
AS 
SELECT ALU_NOMBRE,ALU_APELLIDO,PART_NOMBRE
FROM alumnos AS ALU INNER JOIN partidos AS par
ON ALU.PART_ID=par.PART_ID;

select*from nombreApellidoPartido_view