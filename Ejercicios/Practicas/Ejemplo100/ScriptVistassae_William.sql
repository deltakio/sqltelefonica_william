/* use sae_William
select ALU_NOMBRE, ALU_APELLIDO, PART_NOMBRE 
from alumnos as ALU, partidos as PAR
where ALU.PART_ID=PAR.PART_ID;


select ALU_NOMBRE, ALU_APELLIDO, PART_NOMBRE 
from alumnos as ALU inner join partidos as PAR
on ALU.PART_ID=PAR.PART_ID;

select ALU_NOMBRE, ALU_APELLIDO, PART_NOMBRE, PROV_NOMBRE
from alumnos as ALU inner join partidos as PAR
on ALU.PART_ID=PAR.PART_ID
inner join provincias as PROV
on PAR.PROV_ID=PROV.PROV_ID */

use sae_William
go
create view nombreApellidoPartidoyProvincia_view
as
select ALU_NOMBRE, ALU_APELLIDO, PART_NOMBRE, PROV_NOMBRE
from alumnos as ALU inner join partidos as PAR
on ALU.PART_ID=PAR.PART_ID
inner join provincias as PROV
on PAR.PROV_ID=PROV.PROV_ID

select * from nombreApellidoPartidoyProvincia_view
